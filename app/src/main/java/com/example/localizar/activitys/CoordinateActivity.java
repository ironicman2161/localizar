package com.example.localizar.activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.localizar.R;
import com.example.localizar.controller.Permissions;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class CoordinateActivity extends AppCompatActivity {

    EditText Latitude, Longitude;
    Button Locale, Atual_locale;
    private String[] permissions = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coordinate);
        Latitude = (EditText) findViewById(R.id.latitude_field);
        Longitude = (EditText) findViewById(R.id.longitude_field);
        Locale = (Button) findViewById(R.id.btn_locale);
        Atual_locale = (Button) findViewById(R.id.btn_atual_locale);
        Click();
    }
    private void Click(){
        Locale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String coordinate_latitude = Latitude.getText().toString();
                String coordinate_longitude = Longitude.getText().toString();
                if(TextUtils.isEmpty(coordinate_latitude)) {
                    Latitude.setError("Por Favor preencha este campo");
                    return;
                }
                if(TextUtils.isEmpty(coordinate_longitude)){
                    Longitude.setError("Por Favor preencha este campo");
                    return;
                }
                Log.d("TAGGG", "onClick: "+ coordinate_latitude.toString());
                Intent in = new Intent(v.getContext(), MapsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("longitude", coordinate_longitude);
                in.putExtras(bundle);
                bundle = new Bundle();
                bundle.putString("latitude", coordinate_latitude);
                in.putExtras(bundle);
                startActivity(in);
            }
        });
        Atual_locale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAGG", "onClick: ");
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Permissions.validate(permissions, (Activity) v.getContext(), 1);
                }else{
                    getCurrentLocation();
                }
            }
        });
    }
    @SuppressLint("MissingPermission")
    public void getCurrentLocation(){

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, new LocationCallback(){
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                LocationServices.getFusedLocationProviderClient(CoordinateActivity.this).removeLocationUpdates(this);
                if(locationResult != null && locationResult.getLocations().size()>0){
                    int atual_location = locationResult.getLocations().size() -1;
                    double latitude = locationResult.getLocations().get(atual_location).getLatitude();
                    double longitude = locationResult.getLocations().get(atual_location).getLongitude();
                    Log.d("Latitude e Longitude", "onLocationResult: " + latitude + "   "+ longitude);
                    Latitude.setText(latitude+"");
                    Longitude.setText(longitude+"");

                    Log.d("Latitude e Longitude", "onLocationResult: " + Latitude + "   "+ Longitude);
                }
            }
        }, Looper.getMainLooper());


    }
}